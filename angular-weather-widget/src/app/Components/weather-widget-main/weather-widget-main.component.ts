import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';
import { weatherService } from '../weather-widget.service';
import { ChangeDetectorRef } from '@angular/core';
import { weather, selectWeather } from './weather.interface'; 
import { switchMap, takeUntil } from 'rxjs/operators';
import { Subject, timer } from 'rxjs';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-weather-widget-main',
  templateUrl: './weather-widget-main.component.html',
  styleUrls: ['./weather-widget-main.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeatherWidgetMainComponent implements OnInit, OnDestroy {
  public weatherData:weather;
  private weatherWidgetSubject: Subject<boolean> = new Subject<boolean>();
  public selectCityForm = new FormControl('', [Validators.required]);
  private selectedType: string = 'Celcious';
  public cities: selectWeather[] = [
    {name: 'London', value: 'london'},
    {name: 'New York', value: 'new york'},
    {name: 'Los Angeles', value: 'los angeles'},
    {name: 'Paris', value: 'paris'},
    {name: 'Tokyo', value: 'tokyo'},
  ];
  public degrees: Array<any> = [{
    value:'Celcious',
    checked: true 
  },
  {
    value:'Farenheith', 
    checked: false
  }];
  constructor(private weatherService: weatherService, private ref: ChangeDetectorRef) {}

  public ngOnInit(): void {
    this.getWeatherData('london');
  }

  public ngOnDestroy(): void {
    this.weatherWidgetSubject.next(true);
  }

  public selectedTemperature(evt: any): void {
    this.selectedType = evt.value;
    this.setWeatherData(this.weatherData, this.selectedType);
  }

  public selectCity(event: any): void {
    this.weatherWidgetSubject.next(true);
    this.getWeatherData(event.value);
  }

  private getWeatherData(city: string): void {
    timer(0, 60000).pipe(
      takeUntil(this.weatherWidgetSubject),
      switchMap(() => {
        return this.weatherService.getWeather(city);
      })
    ).subscribe((response) => {
      console.log('triggers');
      this.setWeatherData(response, this.selectedType);
    })
  }

  private setWeatherData(data: weather, tempType: string): void {
    this.weatherData = data;
    let sunsetTime = new Date(this.weatherData.sys.sunset * 1000);
    this.weatherData.sunset_time = sunsetTime.toLocaleTimeString();
    let currentDate = new Date();
    this.weatherData.isDay = (currentDate.getTime() < sunsetTime.getTime());
    if (tempType === "Celcious") {
      this.weatherData.temp_celcius = (this.weatherData.main.temp - 273.15).toFixed(0);
      this.weatherData.temp_min = (this.weatherData.main.temp_min - 273.15).toFixed(0);
      this.weatherData.temp_max = (this.weatherData.main.temp_max - 273.15).toFixed(0);
      this.weatherData.temp_feels_like = (this.weatherData.main.feels_like - 273.15).toFixed(0);
    } else {
      this.weatherData.temp_celcius = ((this.weatherData.main.temp - 273.15) * 9/5 + 32).toFixed(0);
      this.weatherData.temp_min = ((this.weatherData.main.temp_min - 273.15) * 9/5 + 32).toFixed(0);
      this.weatherData.temp_max = ((this.weatherData.main.temp_max - 273.15) * 9/5 + 32).toFixed(0);
      this.weatherData.temp_feels_like = ((this.weatherData.main.feels_like - 273.15) * 9/5 + 32).toFixed(0);
    }
    
    this.ref.markForCheck();
  }

}
