export interface weather {
    base: string;
    clouds: clouds;
    cod: number;
    coord: coord;
    dt: number;
    id: number;
    main: main;
    name: string;
    sys: sys;
    timezone: number;
    visibility: number;
    weather: Array<weatherData>;
    wind: wind;
    sunset_time: string;
    isDay: boolean;
    temp_celcius: string;
    temp_min: string;
    temp_max: string;
    temp_feels_like: string;
}

export interface selectWeather {
    name: string;
    value: string;
}

export interface clouds {
    all: number;
}

export interface coord {
    lat: number;
    lon: number;
}

export interface main {
    feels_like: number;
    humidity: number;
    pressure: number;
    temp: number;
    temp_max: number;
    temp_min: number
}

export interface sys {
    country: string;
    id: number;
    sunrise: number;
    sunset: number;
    type: number;
}

export interface weatherData {
    description: string;
    icon: string;
    id: number;
    main: string;
}

export interface wind {
    deg: number;
    speed: number;
}