import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from "rxjs";
import { weather } from './weather-widget-main/weather.interface';

@Injectable({
    providedIn: 'root',
  })
  export class weatherService {
      constructor (private http: HttpClient) {}

      public getWeather(city: string): Observable<weather> {
          return this.http.get<weather>('https://api.openweathermap.org/data/2.5/weather?q='+ city +'&appid=b7387704f750fa763c36d9e21adc8097');
      }
  }